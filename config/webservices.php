<?php

return array(
	'wsHost' => env('wsProtocol') . env('wsIp') . env('wsUrl'),

	'wsDefaultHeaders' => [
    	'API-Token' => 'DJk6rUcGTf69wAT8',
    	'Host' => env('wsIp'),
	],

);
