<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Password;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Http\Controllers\Controller;

class LoginController extends Controller {


	public function showLogin()
	{
//		if(Request()->headers->get('referer') !== null && strpos(Request()->headers->get('referer'), '/login') === false){
//			Session::put('pre_login_url', Request()->headers->get('referer').'testing');
//		}
//		dd(Request::all());

		return View::make('front.login')
			->with('requested',json_encode(Request::all()));
	}

	public function postLogin()
	{
		$requested=\GuzzleHttp\json_decode((Request::all()['requested']));
		$redirect_uri=$requested->redirect_uri;
		$state=$requested->state;
			$post = Input::all();

		if(!Session::get('pre_login_url') && strlen(Session::get('pre_login_url')) == 0){
			if(strpos(Request()->headers->get('referer'), '/login') === false){
				Session::put('pre_login_url', Request()->headers->get('referer'));
			}else{
				Session::put('pre_login_url', '/');
			}
		}
		$validator = static::validateLoginForm($post);

		if ($validator->fails()) {
			return Redirect::route('login')->withErrors($validator)->withInput();
		} else {
			try {
				$client = new Client();

				$url = \Config::get('webservices.wsHost') . 'login';
				$response = $client->post($url, [
				    'form_params' => [
				        'username' => $post['username'],
				        'password' => $post['password'],
				    ],
				    'headers' => \Config::get('webservices.wsDefaultHeaders')
				]);

				$response = $response->getBody()->getContents();
				$result = json_decode($response,true)['data'];
				$member = $result;

				Session::put('member', $member);
				Session::put('user_token', $member['user_token']);
				if($member['terms'] == '1')
					Session::put('accepted_terms', $member['terms']);


				return Redirect::to($redirect_uri.'?state='.$state);
//				return Redirect::to(Session::pull('pre_login_url', '/'));

        	} catch (RequestException $e) {
        		dd($e);
				$errorDetail = $e->getResponse(); // response from ws.
				$errorDetail = $errorDetail->json();
				Session::flash('errorMessage', $errorDetail['message']);
				return Redirect::route('login')->withInput();
			} catch (\Exception $e) {
				dd($e);
				// dd($e->getMessage());
        		return Redirect::route('login')->withInput();
			}
        }
	}

	private static function validateLoginForm($post)
	{
		$rules = array(
			'username' => 'required',
			'password' => 'required'
		);

		$validator = \Validator::make($post, $rules);

		return $validator;
	}
}
