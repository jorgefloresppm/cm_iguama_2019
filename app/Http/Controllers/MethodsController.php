<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MethodsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */


    public function __construct()
    {
    }

    public function redirect(Request $request)
    {
        $request->session()->put('state', $state = \Str::random(40));

        $query = http_build_query([
            'client_id' => $request->client_id,
            'redirect_uri' => $request->redirect_uri,
            'response_type' => 'code',
            'scope' => $request->scope,
            'state' => $state,
        ]);
        return redirect('oauth/authorize?'.$query);
    }


    public function callback(Request $request)
    {
        $state = $request->session()->pull('state');
        throw_unless(
            strlen($state) > 0 && $state === $request->state,
            InvalidArgumentException::class
        );

        $http = new \GuzzleHttp\Client;
        $response = $http->post('http://localhost/ppm_2019_iguama_cm_autorization/ppm_iguama/public/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $request->client_id,
                'client_secret' => $request->client_secret,
                'redirect_uri' => $request->redirect_uri,
                'code' => $request->code,
            ],
        ]);
        dd($response);
        return json_decode((string) $response->getBody(), true);
    }

}
