
	<div class="container-fluid content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center">Iniciar Sesión</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					@if ( ! empty($errors->all()))
						<div class="alert alert-danger" role="alert">Su código, correo electrónico o contraseña son incorrectos.</div>
					@endif

					@if (Session::has('errorMessage'))
						<div class="alert alert-danger" role="alert">{{ Session::get('errorMessage') }}</div>
					@endif

					@if (Session::has('successMessage'))
                        <div class="alert alert-success" role="alert">{{ Session::get('successMessage') }}</div>
                    @endif

					<form id="login_form" method="POST" action="{{ URL::to('/') }}/login" class="form-ayuda form-ie8">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-6 form-group col-sm-6 col-md-offset-3 col-sm-offset-3{{ $errors->has('username') ? ' has-error' : '' }}">
								<label for="username">Código ClubMiles o correo electrónico:</label>
								<input type="text" name="username" class="form-control" placeholder="Código ClubMiles o email" id="username_content" autocomplete="off" tabindex="3">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-group col-sm-6 col-md-offset-3 col-sm-offset-3{{ $errors->has('password') ? ' has-error' : '' }}">
								<label for="password">Contraseña:</label>
								<input type="password" name="password" class="form-control" placeholder="Contraseña" id="password_content" autocomplete="off" tabindex="4">
							</div>
						</div>
						<input type="hidden" name="requested" id="requested" value={{$requested}}>
						<div class="row">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-info" tabindex="3" id="sesion-inicio">Ingresar</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>

