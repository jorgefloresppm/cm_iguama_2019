<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    dd(Hash::make('123456'));

    return view('welcome');
});


Route::get('/redirect', 'MethodsController@redirect');


Route::get('/callback', 'MethodsController@callback');


Route::get('/revers', function (Request $request) {
    //proceso de revewrso
    //NO EXISTE SCOPE EN IGUAMA PARA REVERSO -CREEEMOS
})->middleware('client:CLIENT_CREDENTIALS');

Route::get('login', array('as' => 'login', 'uses' => 'LoginController@showLogin'));
Route::post('login', array('as' => 'postLogin', 'uses' => 'LoginController@postLogin'));