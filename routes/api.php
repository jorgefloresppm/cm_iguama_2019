<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/redirect', 'MethodsController@redirect');


Route::get('/callback', 'MethodsController@callback');


Route::get('/revers', function (Request $request) {
    //proceso de revewrso
    //NO EXISTE SCOPE EN IGUAMA PARA REVERSO -CREEEMOS
})->middleware('client:CLIENT_CREDENTIALS');

//\Illuminate\Support\Facades\Auth::routes();